LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include $(LOCAL_PATH)/src/agg-2.5/include 
LOCAL_CPPFLAGS += -DMOBILE -DDEBUG

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	src/agg-2.5/src/agg_arc.cpp \
	src/agg-2.5/src/agg_arrowhead.cpp \
	src/agg-2.5/src/agg_bezier_arc.cpp \
	src/agg-2.5/src/agg_bspline.cpp \
	src/agg-2.5/src/agg_curves.cpp \
	src/agg-2.5/src/agg_embedded_raster_fonts.cpp \
	src/agg-2.5/src/agg_gsv_text.cpp \
	src/agg-2.5/src/agg_image_filters.cpp \
	src/agg-2.5/src/agg_line_aa_basics.cpp \
	src/agg-2.5/src/agg_line_profile_aa.cpp \
	src/agg-2.5/src/agg_rounded_rect.cpp \
	src/agg-2.5/src/agg_sqrt_tables.cpp \
	src/agg-2.5/src/agg_trans_affine.cpp \
	src/agg-2.5/src/agg_trans_double_path.cpp \
	src/agg-2.5/src/agg_trans_single_path.cpp \
	src/agg-2.5/src/agg_trans_warp_magnifier.cpp \
	src/agg-2.5/src/agg_vcgen_bspline.cpp \
	src/agg-2.5/src/agg_vcgen_contour.cpp \
	src/agg-2.5/src/agg_vcgen_dash.cpp \
	src/agg-2.5/src/agg_vcgen_markers_term.cpp \
	src/agg-2.5/src/agg_vcgen_smooth_poly1.cpp \
	src/agg-2.5/src/agg_vcgen_stroke.cpp \
	src/agg-2.5/src/agg_vpgen_clip_polygon.cpp \
	src/agg-2.5/src/agg_vpgen_clip_polyline.cpp \
	src/agg-2.5/src/agg_vpgen_segmentator.cpp \
	src/agg-2.5/src/ctrl/agg_bezier_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_cbox_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_gamma_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_gamma_spline.cpp \
	src/agg-2.5/src/ctrl/agg_polygon_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_rbox_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_scale_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_slider_ctrl.cpp \
	src/agg-2.5/src/ctrl/agg_spline_ctrl.cpp \
	src/agg-2.5/src/platform/sdl2/agg_platform_support.cpp \
	src/agg_app.cc src/agg_button_ctrl.cpp src/oware_ai.cc

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_mixer

LOCAL_LDLIBS := -lGLESv1_CM -llog

include $(BUILD_SHARED_LIBRARY)
