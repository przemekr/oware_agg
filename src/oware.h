#ifndef OWARE_H
#define OWARE_H
#include <assert.h>


class Board
{
   protected:
   int pits[12];
   int scoreP0;
   int scoreP1;
   int moveNr; 
   int totalStones; 

   public:
   Board(int noOfStones):
      scoreP0(0), scoreP1(0), moveNr(0)
   {
      for (int i = 0; i < 12; i++)
         pits[i] = noOfStones;
      totalStones = 12*noOfStones;
   }
   Board(const Board& other)
   {
      *this = other;
   }

   static void defaultCallback (int pit, int score)
   {
   }

   int getPit(int pit) {return pits[pit];}
   int getScoreP1() { return scoreP0; }
   int getScoreP2() { return scoreP1; }
   bool endOfTheGame() {return scoreP1 > totalStones/2 || scoreP0 > totalStones/2; }

   bool move_valid(int pit)
   {
      int i;
      int player = moveNr%2;
      /* Check if choosen pit is correct */
      if (pit < 0
            || pit >= (player*6 +6)
            || pit <  (player*6))
         return false; 

      /* Check if player has any stones on his side */
      for (i = player*6; i<player*6 +6; i++)
         if(pits[i]>0)
            break; 

      /* No stones, only empty pits, any move possible */
      if (i == 6*player+6)
         return true;            

      /* Invalid move, empty pit */
      if (pits[pit] == 0)
         return false;    

      return true;
   }

   int currentPlayer()
   {
      return (moveNr%2)? 2: 1;
   }

   bool currentPlayerSide(int pit)
   {
      assert(pit >= 0 && pit <= 11);
      return (moveNr%2 == 0 && pit<6) || (moveNr%2 != 0 && pit >= 6);
   }

   int* currentPlayerScore()
   {
      return (moveNr%2 == 0)? &scoreP0 : &scoreP1;
   }

   bool move(int pit, void (callback)(int pit, int score) = defaultCallback)
   {
      if (!move_valid(pit))
         return false;
      assert(pit >= 0 && pit <= 11);

      int count = pits[pit];
      pits[pit] = 0;
      callback(pit, 0);

      while (count-- > 0)
      {
         pits[++pit%=12]++;
         callback(pit, 0);
      }
      
      /* So, the move was dane, now count the scores.  If the player put his
       * last stone on enemy's side to a pit in which there ware 1 or 2 sotnes.
       * The player gets all the stone from that pit. Moreover if the previous
       * pit is on the enemy side and contain less the 3 he grabs those stones
       * as well, and so on....
       */
      if (1 == pits[pit])
      {
         moveNr++;
         return true;
      }

      while (pits[pit] <= 3 && pit >= 0 && !currentPlayerSide(pit))
      {
         int score = pits[pit];
         *currentPlayerScore() += score;
         pits[pit] = 0;
         callback(pit--, score);
      }
      moveNr++;
      return true;
   }
};

#endif
