#ifndef OWARE_AI_H
#define OWARE_AI_H

#include "oware.h"
#include "ai_minmax_search.h"

class Board_ai: public Board
{
   public:
      int evaluate(int level)
      {
         return (moveNr+level)%2? scoreP1 - scoreP0 : scoreP0 - scoreP1;
      }
      Board_ai(const Board& other)
         :Board(other)
      {
         possibleMovesCount = 0;
      }

      typedef int* iterator;
      iterator begin() { updatePossible(); return possibleMoves; }
      iterator end() { return possibleMoves + possibleMovesCount; }

   private:
      int possibleMoves[6];
      int possibleMovesCount;
      void updatePossible()
      {
         possibleMovesCount = 0;
         for (int i = moveNr%2*6; i < moveNr%2*6+6; i++)
            if (move_valid(i))
               possibleMoves[possibleMovesCount++] = i;
      }
};

int ai_move(Board b, int level);

#endif
