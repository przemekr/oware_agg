/*
 * Oware, the acient African game.
 * Copyright 2013 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of Oware.
 * 
 * Oware is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * Oware is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Oware.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stack>
#include "agg_basics.h"
#include "agg_rendering_buffer.h"
#include "agg_rasterizer_scanline_aa.h"
#include "agg_scanline_u.h"
#include "agg_scanline_p.h"
#include "agg_renderer_scanline.h"
#include "agg_ellipse.h"
#include "agg_pixfmt_gray.h"
#include "agg_pixfmt_rgb.h"
#include "agg_pixfmt_rgba.h"
#include "agg_pixfmt_amask_adaptor.h"
#include "agg_span_allocator.h"
#include "agg_span_gradient.h"
#include "agg_span_interpolator_linear.h"
#include "agg_conv_transform.h"
#include "agg_color_rgba.h"
#include "agg_gsv_text.h"
#include "ctrl/agg_slider_ctrl.h"
#include "agg_button_ctrl.h"
#include "platform/agg_platform_support.h"
#include <unistd.h>
#include <stdint.h>
#include "stdio.h"
#include "oware.h"
#include "oware_ai.h"

#ifdef MOBILE
#define START_H  500
#define START_W  900
#define WINDOW_FLAGS agg::window_fullscreen | agg::window_keep_aspect_ratio
#define CTRL_TEXT_THICKNESS 2
#define S 10
#else
#define START_H  400
#define START_W  800
#define CTRL_TEXT_THICKNESS 1
#define WINDOW_FLAGS agg::window_resize
#define S 10
#endif

#if __ANDROID__
#include <android/log.h>
#define DEBUG_PRINT(...) do{ __android_log_print(ANDROID_LOG_INFO, __FILE__, __VA_ARGS__ ); } while (false)
#else
#define DEBUG_PRINT(...) do{fprintf(stderr, __VA_ARGS__ ); } while (false)
#endif

enum flip_y_e { flip_y = true };
const agg::rgba transp(0, 0, 0, 0);
const agg::rgba lblue(0,0,128,128);
const agg::rgba lgray(40,40,40);
const agg::rgba green(0,120,0);
const agg::rgba yellow(63,63,0);
const agg::rgba red(120,0,0);
const agg::rgba black(0,0,0);

#define PLAIN       0
#define POINTED_OK  1
#define POINTED_NOK 2
#define TARGETED    3
#define HILIGHTED   4
#define MOVED       5
#define SCORED      6

class Application;
Application* application;

void draw_text(agg::renderer_base<agg::pixfmt_bgra32> rb,
      double x, double y, double size, const char* str, int attr)
{
   typedef agg::pixfmt_bgra32 pixfmt_type;
   agg::rasterizer_scanline_aa<> m_ras;
   agg::scanline_p8              m_sl;
   agg::renderer_scanline_aa_solid<agg::renderer_base<pixfmt_type> > ren(rb);

   agg::gsv_text txt;
   agg::conv_stroke<agg::gsv_text> txt_stroke(txt);
   txt_stroke.width(size/6);
   txt_stroke.line_cap(agg::round_cap);
   txt.size(size);
   txt.start_point(x, y);
   txt.text(str);
   m_ras.add_path(txt_stroke);

   if (attr == HILIGHTED)
      ren.color(agg::rgba(0, 0, 0.8, 1.0));
   else 
      ren.color(agg::rgba(0, 0, 0.6, 0.8));

   agg::render_scanlines(m_ras, m_sl, ren);
}

agg::rgba white(0.1, 0.1, 0.1, 0.9);

class Pit
{
public:
   int nr;
   int lastScore;
   Board* board;
   Application* app;
   agg::rgba lastColor;
   int fade;
   
   void draw(agg::renderer_base<agg::pixfmt_bgra32> rbase,
         double x, double y, double r1, double r2, int attr, int score)
   {
      agg::rasterizer_scanline_aa<> ras;
      typedef agg::pixfmt_bgra32 pixfmt_type;
      typedef agg::renderer_base<pixfmt_type>                     renderer_base_type;
      typedef agg::renderer_scanline_aa_solid<renderer_base_type> renderer_scanline_type;
      renderer_scanline_type ren_sl(rbase);
      renderer_scanline_type ren_sl2(rbase);
      renderer_scanline_type ren_sl3(rbase);
      ren_sl.color(agg::rgba(1, 1, 1, 0.7));
      double g = std::max(0.0, 1-1.0*fade++/20);
      switch (attr)
      {
         case PLAIN:
            ren_sl2.color(white.gradient(lastColor, g));
            break;
         case POINTED_OK:
            ren_sl2.color(agg::rgba(0.1, 0.9, 0.1, 0.9));
            break;
         case POINTED_NOK:
            ren_sl2.color(agg::rgba(0.9, 0.1, 0.1, 0.9));
            break;
         case TARGETED:
            ren_sl2.color(agg::rgba(0.9, 0.1, 0.9, 0.9));
            break;
         case MOVED:
            lastColor = agg::rgba(0.9, 0.1, 0.9, 0.9);
            ren_sl2.color(lastColor);
            fade = 0;
            break;
         case SCORED:
            lastColor = agg::rgba(0.1, 0.9, 0.1, 0.9);
            ren_sl2.color(lastColor);
            fade = 0;
            lastScore = score;
            break;
      }
      agg::scanline_u8 sl;

      agg::ellipse e;
      ras.reset();
      e.init(x, y, r1, r2, 64);
      ras.add_path(e);
      agg::render_scanlines(ras, sl, ren_sl);

      ras.reset();
      e.init(x, y, r1*0.95, r2*0.95, 64);
      ras.add_path(e);
      agg::render_scanlines(ras, sl, ren_sl2);

      ras.reset();
      e.init(x, y, r1*0.9, r2*0.9, 64);
      ras.add_path(e);
      agg::render_scanlines(ras, sl, ren_sl);

      ren_sl3.color(agg::rgba(0.05, 0.05, 0.05, 1));
      for (int i = 1; i <= board->getPit(nr); i++)
      {
         ras.reset();
         double mx = x + getStonePos(i, board->getPit(nr), r1, 'x');
         double my = y + getStonePos(i, board->getPit(nr), r2, 'y');
         e.init(mx, my, r1*0.27, r2*0.27, 255);
         ras.add_path(e);
         agg::render_scanlines(ras, sl, ren_sl3);
      }

      if (board->getPit(nr) >= 10)
      {
         ras.reset();
         e.init(x, y, r1*0.9, r2*0.9, 30);
         ras.add_path(e);
         agg::render_scanlines(ras, sl, ren_sl);
         char buf[20]; sprintf(buf, "%d", board->getPit(nr));
         draw_text(rbase, x-0.7*r1, y-0.7*r2/2, 0.7*r1, buf, PLAIN);
      }

      if (fade < 20 && lastScore)
      {
         char buf[20]; sprintf(buf, "+%d", lastScore);
         draw_text(rbase, x-0.8*r1, y-0.8*r2/2+0.3*fade, 0.4*r1, buf, HILIGHTED);
      }
      if (fade == 20)
      {
         lastScore = 0;
      }
   }

   double getStonePos(int nth, int total, double r, char cordinate)
   {
      if (nth == 1 && total == 1)
         return 0;
      if (nth == 1 && total >= 5 && total < 10)
         return 0;
      if (total < 5) r *= 0.95;

      double angle = 2*M_PI/( total >= 9? 8: total >= 5? total-1: total);
      double a_shift = (total %2) ? M_PI/7: M_PI/10;

      if (nth > 9)
      {
         r *= 0.4;
         angle = 2*M_PI/(total- 9);
      }

      switch (cordinate)
      {
         case 'x':
            return 0.65*r*sin(nth*angle + a_shift);
         case 'y':
            return 0.65*r*cos(nth*angle + a_shift);
         default:
            return 0.0;
      }
   }
};

class Application : public agg::platform_support
{
public:
   Application(agg::pix_format_e format, bool flip_y) :
      agg::platform_support(format, flip_y),
      newg(0, 0,   50, 20,   "NEW",  !flip_y),
      undo(0, 30,  50, 50,   "UNDO", !flip_y),
      p1_ctr(100, 5,  240,   20,  !flip_y),
      p2_ctr(100, 35, 240,   50,  !flip_y),
      movePit(-1), moveScore(0), moveFrame(0), lastClick(false),
      board(4), pointed_pit(-1), targeted_pit(-1)
   {
      lx = ly = 0;
      w = int(rbuf_window().width());
      h = int(rbuf_window().height());

      p1_ctr.range(0, 10);
      p1_ctr.num_steps(10);
      p1_ctr.value(0);
      p1_ctr.label("P1=%1.0f");
      p1_ctr.background_color(transp);
      p1_ctr.text_color(lgray);
      p1_ctr.text_thickness(CTRL_TEXT_THICKNESS);

      p2_ctr.range(0, 10);
      p2_ctr.num_steps(10);
      p2_ctr.value(0);
      p2_ctr.label("P2=%1.0f");
      p2_ctr.background_color(transp);
      p2_ctr.text_color(lgray);
      p2_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      add_ctrl(p1_ctr);
      add_ctrl(p2_ctr);
      add_ctrl(newg);
      add_ctrl(undo);

      for (int i = 0; i < 12; i++)
      {
         pits[i].nr = i;
         pits[i].fade = 0;
         pits[i].lastColor = white;
         pits[i].lastScore = 0;
         pits[i].board = &board;
      }
   }

   virtual void on_ctrl_change()
   {
      if (board.endOfTheGame())
         wait_mode(true);
      else if (board.currentPlayer() == 1 && p1_ctr.value())
      {
         wait_mode(false);
         undoList.push(board);
         board.move(ai_move(board, p1_ctr.value()), moveCallback);
      }
      else if (board.currentPlayer() == 2 && p2_ctr.value())
      {
         wait_mode(false);
         undoList.push(board);
         board.move(ai_move(board, p2_ctr.value()), moveCallback);
      }
      else
         wait_mode(true);

      if (undo.status())
      {
         undo.status(false);
         if (!undoList.empty())
         {
            board = undoList.top(); undoList.pop();
            if (board.currentPlayer() == 1 && p1_ctr.value()
                  || board.currentPlayer() == 2 && p2_ctr.value())
            {
               board = undoList.top(); undoList.pop();
            }
         }
      }
      if (newg.status())
      {
         newg.status(false);
         board = Board(4);
      }
      force_redraw();
   }

   virtual void on_resize(int, int)
   {
      force_redraw();
      w = int(rbuf_window().width());
      h = int(rbuf_window().height());
   }


   virtual void on_idle()
   {
      if (board.endOfTheGame())
         wait_mode(true);
      else if (board.currentPlayer() == 1 && p1_ctr.value())
      {
         undoList.push(board);
         board.move(ai_move(board, p1_ctr.value()), moveCallback);
      }
      else if (board.currentPlayer() == 2 && p2_ctr.value())
      {
         undoList.push(board);
         board.move(ai_move(board, p2_ctr.value()), moveCallback);
      }
      else if (std::max(0, moveFrame--) == 0)
         wait_mode(true);

      force_redraw();
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      pointed_pit = -1;
      targeted_pit = -1;
      if (!lastClick)
      {
         lastClick = true;
         start_timer();
         return;
      }
      lastClick = false;
      if (elapsed_time() > 1000)
      {
         return;
      }

      int pit = pix_to_pit(x, y);
      if (board.move_valid(pit))
      {
         undoList.push(board);
         board.move(pit, moveCallback);
      }
      force_redraw();
      wait_mode(false);
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      int pit = pix_to_pit(x, y);
      if (pit != pointed_pit)
      {
         pointed_pit = pit;
         targeted_pit = (pit + board.getPit(pit)) % 12; 
         force_redraw();
      }
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (! flags & agg::mouse_left)
         return;

      int pit = pix_to_pit(x, y);
      if (pit != pointed_pit)
      {
         pointed_pit = pit;
         targeted_pit = (pit + board.getPit(pit)) % 12; 
         force_redraw();
      }
   }
   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
   }
   virtual void on_touch_event(float x, float y, float dx, float dy, int id, bool down) {}

   virtual void on_draw()
   {
      agg::pixfmt_bgra32 pf(rbuf_window());;
      agg::renderer_base<agg::pixfmt_bgra32> rbase(pf);
      agg::scanline_u8 sl;
      agg::rasterizer_scanline_aa<> ras;
      rbase.clear(black);

      double shift = 0.05* w;
      double size = (w - 2*shift)/6;
      shift += size/2;
      for (int i = 0; i < 12; i++)
      {
         int attr = PLAIN;
         if (i == pointed_pit && board.move_valid(i))
            attr = POINTED_OK;
         if (i == pointed_pit && !board.move_valid(i))
            attr = POINTED_NOK;
         if (i != pointed_pit && i == targeted_pit)
            attr = TARGETED;
         if (i == movePit)
            attr = MOVED;
         if (i == movePit && moveScore)
            attr = SCORED;


         double x = shift+ (i < 6? i*size: (11-i)*size);
         double y = shift+ (i < 6? 0: 2*size); 
         pits[i].draw(rbase, x, y, 0.9*size/2, 0.9*size/2, attr, moveScore);
      }

      char buf[50];
      sprintf(buf, "P1: %d", board.getScoreP1());
      draw_text(rbase, shift/2, 2*shift-shift/3, shift/3, buf, board.currentPlayer() == 1? HILIGHTED: PLAIN);
      sprintf(buf, "P2: %d", board.getScoreP2());
      draw_text(rbase, shift/2, 2*shift+shift/3, shift/3, buf, board.currentPlayer() == 2? HILIGHTED: PLAIN);

      double scale = rbuf_window().width()/400.0;
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scale);
      shape_mtx *= agg::trans_affine_translation(2.5*shift, 1.666*shift);
      p1_ctr.transform(shape_mtx);
      p2_ctr.transform(shape_mtx);
      undo.transform(shape_mtx);
      newg.transform(shape_mtx);

      agg::render_ctrl(ras, sl, rbase, p1_ctr);
      agg::render_ctrl(ras, sl, rbase, p2_ctr);
      agg::render_ctrl(ras, sl, rbase, undo);
      agg::render_ctrl(ras, sl, rbase, newg);
   }

private:
   Board board;
   std::stack<Board> undoList;
   int pointed_pit, targeted_pit;
   int w, h;
   int lx, ly; 
   agg::slider_ctrl<agg::rgba8> p1_ctr;
   agg::slider_ctrl<agg::rgba8> p2_ctr;
   agg::button_ctrl<agg::rgba8> undo;
   agg::button_ctrl<agg::rgba8> newg;

   int pix_to_pit(int x, int y)
   {
      double shift = 0.05* w;
      double size = (w - 2*shift)/6;
      int row, pos;
      if (y > shift && y < shift+size)
         row = 1; 
      else if (y > shift+ 2*size && y < shift+3*size)
         row = 2; 
      else
         return -1;

      pos = (x - shift)/size;

      if (pos < 0 || pos > 5)
         return -1;

      return (row == 1)? pos : 11 - pos;
   }

   static void moveCallback(int pit, int score)
   {
      const int MAX_FPS = 20;
      application->start_timer();
      application->movePit = pit;
      application->moveScore  = score;
      for (int i = 0; i < 5; i++)
      {
         application->on_draw();
         application->update_window();
         int loop_time = application->elapsed_time();
         if (loop_time < 1000/MAX_FPS)
         {
            usleep(1000*(1000/MAX_FPS-loop_time));
         }
      }
      application->movePit = -1;
      application->moveScore  = 0;
      application->moveFrame  = 20;
   }

   int movePit;
   int moveScore;
   int moveFrame;
   bool lastClick;
   Pit pits[12];
};

int agg_main(int argc, char* argv[])
{
   Application app(agg::pix_format_bgra32, flip_y);
   app.caption("Oware");
   application = &app;
   if (app.init(START_W, START_H, WINDOW_FLAGS))
   {
      return app.run();
   }
   return 1;
}
