#include "ai_minmax_search.h"
#include "oware_ai.h"
#include "stdio.h"

typedef Node<Board_ai, int> OwareAI;

template <typename B, typename M>
M Node<B, M>::move_list[15];
template <typename B, typename M>
Node<B, M>* Node<B, M>::next_free;

int ai_move(Board b, int level)
{
   return OwareAI::ai_move(b, level);
}

int __main(int argc, char* argv[])
{
   Board b(3);
   int move;

   while (true)
   {
      move = ai_move(b, 7);
      printf("P1 move: %d\n", move);
      b.move(move);
      if (b.endOfTheGame())
         break;

      move = ai_move(b, 4);
      printf("P2 move: %d\n", move);
      b.move(move);
      if (b.endOfTheGame())
         break;
   }
   printf("end of the game P1:%d, P2:%d\n", b.getScoreP1(), b.getScoreP2());
}
